# Tizen Techz #

This is a static web application built using HTML, CSS, JavaScript JQuery. This application allows the users to become efficient in
technologies like HTML, CSS, JQuery, C PHP

### Environment ###

* HTML
* CSS
* JavaScript
* JQuery
* Bootstrap


### How do I get set up? ###

Just clone the repo, navigate to the project folder and open the "home.html" in your browser

