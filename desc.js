// JavaScript Document
$(function(){

$( document ).on( "mobileinit", function() {
    $.mobile.loader.prototype.options.disabled = true;$.mobile.loading( "hide" );
});

var cat=document.location.search.substring(1).split('&')[0].split('=')[1];
var str=document.location.search.substring(1).split('&')[1].split('=')[1];

switch(cat){
	case "css":$('#ti').text("CSS");$('#nav').css('background-color','#16C587');break;
	case "js":$('#ti').text("JavaScript");$('#nav').css('background-color','#E94D26');break;
	case "html":$('#ti').text("HTML");$('#nav').css('background-color','#E68443');break;
	case "php":$('#ti').text("PHP");$('#nav').css('background-color','#03ABFF');break;
	case "jquery":$('#ti').text("JQUERY");$('#nav').css('background-color','#418AD4');break;
	case "c":$('#ti').text("C");$('#nav').css('background-color','#0C3');break;
	}
$('#back').click( function(){
	window.location.href=cat+'.html';
	});
$('img').on('dragstart', function(event) { event.preventDefault(); });

$('a').click(function(){
	 if($(this).text()=='Reference'){
	$('#anch1').siblings('a').removeClass('bottom').removeClass('selected').end().addClass('bottom').addClass('selected');
	$('#tab-ref').css('display','block');
	$('#tab-prog').css('display','none');
	}
	else{
		$('#anch2').siblings('a').removeClass('bottom').removeClass('selected').end().addClass('bottom').addClass('selected').css('padding-right', '11%').css('padding-left','11%');
	$('#tab-prog').css('display','block');
	$('#tab-ref').css('display','none');
	}
});

$('#anch1').addClass('bottom').addClass('selected');
$('#tab-ref').html(getText(cat,str));
$('#tab-prog').children('img').attr('src',getImage(cat,str));

$(document).bind('swiperight',function (event){
	$('#anch1').siblings('a').removeClass('bottom').removeClass('selected').end().addClass('bottom').addClass('selected');
	$('#tab-ref').css('display','block');
	$('#tab-prog').css('display','none');
	console.log('Right');
});
$(document).bind('swipeleft',function (event){
	$('#anch2').siblings('a').removeClass('bottom').removeClass('selected').end().addClass('bottom').addClass('selected').css('padding-right', '12%').css('padding-left','11%');
	$('#tab-prog').css('display','block');
	$('#tab-ref').css('display','none');console.log('Left');
});
$('a').click(function(){
	var temp=$(this).text().toLowerCase();
	temp=='reference' ? $(this).siblings('a').removeClass('bottom').end().addClass('bottom') : $(this).addClass('bottom').siblings('a').removeClass('bottom').end();	
	});
	
function getText(val,key){
	var text="";
	switch(val){
		case "css": text=cssRef(key);break;
		case "js": text=jsRef(key);break;
		case "html": text=htmlRef(key);break;
		case "php":text=phpRef(key);break;
		case "jquery":text=jqueryRef(key);break;
		case "c":text=cRef(key);break;
		}
		return text;
	}
function getImage(val,key){
	var image="";
	switch(val){
		case "css": image=cssEx(key);break;
		case "js": image=jsEx(key);break;
		case "html": image=htmlEx(key);break;
		case "php":image=phpEx(key);break;
		case "jquery":image=jqueryEx(key);break;
		case "c":image=cEx(key);break;		
		}
	return image;
	}

function cssEx(key){
	var image="/ExamplesCSS/";
	if(key=="syntax")
	{
	 	image+="cssSyntax1.png";
	}
	else if(key=="selectors")
	{
	 	image+="cssSelectors1.png";
	}
		else if(key=="insert")
	{
	 	image+="insertCSS1.png";
	}
	else if(key=="color")
	{
	 	image+="colors1.png";
	}
	else if(key=="font")
	{
	 	image+="fonts1.png";
	}
	else if(key=="margin")
	{
	 	image+="margin1.png";
	}
	else if(key=="padding")
	{
	 	image+="Padding1.png";
	}
	else if(key=="position")
	{
	 	image+="position1.png";
	}
	else if(key=="float")
	{
	 	image+="float.png";
	}
	else if(key=="transition")
	{
	 	image+="transition1.png";
	}
	return image;
	}
function cssRef(key){
	var text="";
	if(key=="syntax"){
	 	text="<h1>CSS Syntax</h1><p>A CSS rule set consists of a selector and a declaration block,A CSS declaration always ends with a semicolon, and declaration groups are surrounded by curly braces.</p>";
	}
   else if(key == "selectors"){
		text="<h1>CSS Selectors</h1><p>CSS selectors allow you to select and manipulate HTML elements.CSS selectors are used to 'find' (or select) HTML elements based on their id, class, type, attribute, and more.</p><h4>Element Selector</h4><p>The element selector selects elements based on the element name.</p><h4>Id Selector</h4><p>The id selector uses the id attribute of an HTML element to select a specific element.</p><p>    An id should be unique within a page, so the id selector is used if you want to select a single, unique element.</p><p>    To select an element with a specific id, write a hash character, followed by the id of the element.</p><h4>Class Selector</h4><p>The class selector selects elements with a specific class attribute.</p><p>	To select elements with a specific class, write a period character, followed by the name of the class</p>";		
		}
	else if(key=="insert"){
		text="<h1>How to CSS</h1><h4>External</h4><p>With an external style sheet, you can change the look of an entire website by changing just one file!</p><p>	Each page must include a reference to the external style sheet file inside the <link> element. The <link> element goes inside the head section</p><h4>Internal</h4><p>An internal style sheet may be used if one single page has a unique style.</p><p>		Internal styles are defined within the <style> </style>element, inside the head section of an HTML page </p><h4>Inline</h4><p>An inline style may be used to apply a unique style for a single element.</p><p>		An inline style loses many of the advantages of a style sheet (by mixing content with presentation). Use this method sparingly!</p><p>		To use inline styles, add the style attribute to the relevant tag. The style attribute can contain any CSS property.</p>";
		}
	else if(key=="color"){
		text="<h1>Colors</h1><h4>Background</h4><p>The 'background-color' property specifies the background color of an element.</p><h4>Foreground</h4><p>The 'color' property specifies the foreground color of an element.</p>";
		}
	else if(key=="font"){
		text="<h1>Font</h1><p>CSS font properties define the font family, boldness, size, and the style of a text.</p><h4>Font-Family</h4>	<p>The font family of a text is set with the font-family property.</p><p>	The font-family property should hold several font names as a 'fallback' system. If the browser does not support the first font, it tries the next font.	Start with the font you want, and end with a generic family, to let the browser pick a similar font in the generic family, if no other fonts are available.</p><p>		Note: If the name of a font family is more than one word, it must be in quotation marks, like: 'Times New Roman'.</p><h4>Font-Style</h4>		<p>The font-style property is mostly used to specify italic text.		This property has three values:<br/>	normal - The text is shown normally<br/>		    italic - The text is shown in italics<br/>		    oblique - The text is 'leaning' (oblique is very similar to italic, but less supported)</p><h4>FontSize</h4>		<p>The font-size property sets the size of the text.<br/>		Being able to manage the text size is important in web design. However, you should not use font size adjustments to make paragraphs look like headings, or headings look like paragraphs.</p>";
		}
	else if(key=="margin"){
		text="<h1>Margin</h1>	<p>The margin clears an area around an element (outside the border). The margin does not have a background color, and is completely transparent.<br/>	The top, right, bottom, and left margin can be changed independently using separate properties. A shorthand margin property can also be used, to change all margins at once.</p> <table cellspacing='0' style='border-spacing: 0;'> <tbody><tr>    <th style='width:20%'>Property</th>    <th>Description</th>  </tr>  <tr>    <td>margin</td>    <td>A shorthand property for setting the margin properties in one declaration</td>  </tr>  <tr>    <td>margin-bottom</td>    <td>Sets the bottom margin of an element</td>  </tr>  <tr>    <td>margin-left</td>    <td>Sets the left margin of an element</td>  </tr>  <tr>    <td>margin-right</td>    <td>Sets the right margin of an element</td>  </tr>  <tr>    <td>margin-top</td>    <td>Sets the top margin of an element</td>  </tr></tbody></table>"	;
		}
	else if(key=="padding"){
		text="<h1>Padding</h1><p>The CSS padding properties define the space between the element border and the element content.</p>	<p>The padding clears an area around the content (inside the border) of an element. The padding is affected by the background color of the element.</p>	<p>The top, right, bottom, and left padding can be changed independently using separate properties. A shorthand padding property can also be used, to change all paddings at once.</p><table cellspacing='0' style='border-spacing: 0;'>  <tbody><tr>    <th style='width:20%'>Property</th>    <th>Description</th>  </tr>  <tr>    <td>padding</td>    <td>A shorthand property for setting all the padding properties in one declaration</td>  </tr>  <tr>   <td>padding-bottom</td>    <td>Sets the bottom padding of an element</td>  </tr>  <tr>    <td>padding-left</td>    <td>Sets the left padding of an element</td>  </tr>  <tr>    <td>padding-right</td>    <td>Sets the right padding of an element</td>  </tr>  <tr>    <td>padding-top</td>    <td>Sets the top padding of an element</td></tr></tbody></table>";
		}
	else if(key=="position"){
		text="<h1>Position</h1>	<p>The position property specifies the type of positioning method used for an element (static, relative, fixed or absolute).</p>	<p>The position property specifies the type of positioning method used for an element.<br/>	Elements are then positioned using the top, bottom, left, and right properties. However, these properties will not work unless the position property is set first. They also work differently depending on the position value.</p>	<p><b>position: static</b><br/>	HTML elements are positioned static by default.	Static positioned elements are not affected by the top, bottom, left, and right properties.<br/>	An element with position: static; is not positioned in any special way; it is always positioned according to the normal flow of the page.</p>	<p><b>position: relative;</b><br/>	An element with position: relative; is ositioned relative to its normal position.<br/>	Setting the top, right, bottom, and left properties of a relatively-positioned element will cause it to be adjusted away from its normal position. Other content will not be adjusted to fit into any gap left by the element.</p>	<p><b>position: fixed;</b><br/>	An element with position: fixed; is positioned relative to the viewport, which means it always stays in the same place even if the page is scrolled. The top, right, bottom, and left properties are used to position the element.<br/>	A fixed element does not leave a gap in the page where it would normally have been located.</p>	<p><b>position: absolute;</b><br/>	An element with position: absolute; is positioned relative to the nearest positioned ancestor (instead of positioned relative to the viewport, like fixed).<br/>	However; if an absolute positioned element has no positioned ancestors, it uses the document body, and moves along with page scrolling.</p>";
		}
	else if(key=="float"){
		text="<h1>Float</h1><p>The float property specifies whether or not an element should float.<br/>		In its simplest use, the float property can be used to wrap text around image.	</p>";
		}
	else if(key=="transition"){
		text="<h1>Transition</h1><p>CSS3 transitions allows you to change property values smoothly (from one value to another), over a given duration.</p>	<p>To create a transition effect, you must specify two things:<br/>&#9755	    the CSS property you want to add an effect to<br/>&#9755    the duration of the effect</p>	<p>Note: If the duration part is not specified, the transition will have no effect, because the default 	value is 0.</p><table cellspacing='0' style='border-spacing: 0;'>  <tbody><tr>    <th>Property</th>    <th>Description</th>  </tr>  <tr>    <td>transition</td>    <td>A shorthand property for setting the four transition properties into a single property</td>  </tr>	<tr>    <td>transition-delay</td>    <td>Specifies a delay (in seconds) for the transition effect</td>    </tr>	<tr>    <td>transition-duration</td>    <td>Specifies how many seconds or milliseconds a transition effect takes to complete</td>    </tr>	<tr>    <td>transition-property</</td>    <td>Specifies the name of the CSS property the transition effect is for</td>  </tr>	<tr>    <td>transition-timing-function</td>    <td>Specifies the speed curve of the transition effect</td>  </tr></tbody></table>";
		}
	return text;
	}
	
function jsRef(key){
	var text="";
	if(key=="introduction"){
	    text="<h1>JavaScript</h1><h4>Why Study?</h4><p>JavaScript is one of the 3 languages all web developers must learn:<br>1. HTML to define the content of web pages<br>2. CSS to specify the layout of web pages<br>3. JavaScript to program the behavior of web pages<br> </p><h4>JavaScript Programs</h4><p>A computer program is a list of 'instructions' to be 'executed' by the computer.In a programming language, these program instructions are called statements.JavaScript is a programming language.avaScript statements are separated by semicolons.</p><p>JavaScript statements are composed of:Values, Operators, Expressions, Keywords, and Comments.</p>";
	}
	else if(key=="alert"){
	    text="<h1>Alert</h1><p>The alert() method displays an alert box with a specified message and an OK button.An alert box is often used if you want to make sure information comes through to the user.</p><p><b>Note</b>: The alert box takes the focus away from the current window, and forces the browser to read the message. Do not overuse this method, as it prevents the user from accessing other parts of the page until the box is closed.	</p>";
	}
	else if(key=="prompt"){
	    text="<h1>Prompt</h1><p>The prompt() method displays a dialog box that prompts the visitor for input.A prompt box is often used if you want the user to input a value before entering a page.<br>Note: When a prompt box pops up, the user will have to click either 'OK' or 'Cancel' to proceed after entering an input value. Do not overuse this method, as it prevent the user from accessing other parts of the page until the box is closed.<br>The prompt() method returns the input value if the user clicks 'OK'. If the user clicks 'cancel' the method returns null.</p>";
	}
	else if(key=="continue"){
	   text="<h1>Continue</h1><p>The continue statement breaks one iteration (in the loop), if a specified condition occurs, and continues with the next iteration in the loop.</p>";
	}
	else if(key=="write"){
	    text="<h1>write method</h1><p>The write() method writes HTML expressions or JavaScript code to a document.The write() method is mostly used for testing: If it is used after an HTML document is fully loaded, it will delete all existing HTML.<br>Note: When this method is not used for testing, it is often used to write some text to an output stream opened by the document.open() method.</p>";
	}
	else if(key=="datatypes"){text="<h1>Java Script Data Types</h1><p>In programming, data types is an important concept.To be able to operate on variables, it is important to know something about the type.Without data types, a computer cannot safely solve anything.<br>JavaScript evaluates expressions from left to right. Different sequences can produce different results.<br>JavaScript has dynamic types.This means that the same variable can be used as different types.<br></p><h4>JavaScript Strings</h4><br><p>A string (or a text string) is a series of characters like 'John Doe'.Strings are written with quotes. You can use single or double quotes.</p>";
	}
	
	else if(key=="function"){
	    text="<h1>Function</h1><p>A JavaScript function is a block of code designed to perform a particular task.A JavaScript function is executed when 'something' invokes it (calls it).</p><h4>Function Syntax</h4><p>A JavaScript function is defined with the function keyword, followed by a name, followed by parentheses ().Function names can contain letters, digits, underscores, and dollar signs (same rules as variables).The parentheses may include parameter names separated by commas: (parameter1,  parameter2, ...).The code to be executed, by the function, is placed inside curly brackets: {}<br>Function parameters are the names listed in the function definition.Function arguments are the real values received by the function when it is invoked.Inside the function, the arguments behaves as local variables.</p><h4>Function Invocation</h4><p>The code inside the function will execute when 'something' invokes (calls) the function:<br>1)When an event occurs (when a user clicks a button)<br>2)When it is invoked (called) from JavaScript code<br>3)Automatically (self invoked)<br></p>"; 
	}
	else if(key=="if"){
	    text="<h1>If Else Statements</h1><h4> If statement</h4><p>Use the if statement to specify a block of JavaScript code to be executed if a condition is true.</p>";
	}
	else if(key=="switch"){
	    text="<h1>Switch</h1><p>Use the switch statement to select one of many blocks of code to be executed.<br>This is how it works:<br>1)The switch expression is evaluated once.<br>2)The value of the expression is compared with the values of each case.<br>3)If there is a match, the associated block of code is executed.<br></p>";
	}
	else if(key=="for"){
	    text="<h1>For Loop</h1><p>The for loop is often the tool you will use when you want to create a loop.</p>";
	}
	else if(key=="else"){
	    text="<h4>Else statement</h4><p> The else statement is used to specify a block of code to be executed if the condition is false.</p>";
	}
	else if(key=="while"){
	    text="<h1>While</h1><p>The while loop loops through a block of code as long as a specified condition is true.</p>";
	}
	return text;
	}
function jsEx(key){
	var image="/ExamplesJS/";
	if(key=="introduction"){
		image+="";
	}
	else if(key=="alert"){
		image+="Alert.png";
	}
	else if(key=="prompt"){
		image+="Prompt.png";
	}
	else if(key=="write"){
		image+="write.png";
	}
	else if(key=="switch"){
		image+="Switch.png";
		}
	else if(key=="datatypes"){
		image+="data_types.png";
	}
	else if(key=="while"){
		image+="while.png";
	}
	else if(key=="for"){
		image+="for.png";
	}
	else if(key=="if"){
		image+="if_else.png";
	}
	else if(key=="else"){
		image+="if_else.png";
	}
	 return image;
	}
	

function htmlRef(key){
    var text="";
if(key=="doctype"){
	text="<h1>Doctype</h1><p>The <strong>DOCTYPE</strong> declaration defines the document type to be HTML</p><p>The &lt;!DOCTYPE&gt; declaration is not an HTML tag; it is an instruction to the web browser about what version of HTML the page is written in.</p>";
}
else if(key=="html"){
	text="<h1>HTML</h1><p>The text between <strong>&lt;html&gt;</strong> and <strong>&lt;/html&gt;</strong> describes an HTML document</p><p>The &lt;html&gt; tag is the container for all other HTML elements (except for the &lt;!DOCTYPE&gt; tag).</p>";
}
else if(key=="head"){
	text="<h1>Head</h1><p> The text between <strong>&lt;head&gt;</strong> and <strong>&lt;/head&gt;</strong> provides information about the document     </p><p>The &lt;head&gt; element can include a title for the document, scripts, styles, meta information, and more.</p>";
}
else if(key=="title"){
	text="<h1>Title</h1><p> The text between <strong>&lt;title&gt;</strong> and <strong>&lt;/title&gt;</strong> provides a title for the document </p><p>The &lt;title&gt; element:</p><ul>	<li>defines a title in the browser toolbar</li>	<li>provides a title for the page when it is added to favorites</li>	<li>displays a title for the page in search-engine results</li></ul>";
}
else if(key=="body"){
	text="<h1>Body</h1><p>The text between <strong>&lt;body&gt;</strong> and <strong>&lt;/body&gt;</strong> describes the visible page content</p>";
}
else if(key=="para"){
	text="<h1>Paragraph</h1><p>The text between <strong>&lt;p&gt;</strong> and <strong>&lt;/p&gt;</strong> describes a paragraph </p><p>The &lt;body&gt; element contains all the contents of an HTML document, such as text, hyperlinks, images, tables, lists, etc.</p>";
}
else if(key=="break"){
	text="<h1>Break</h1><p>The &lt;br&gt; tag inserts a single line break.&nbsp;</p><p>The &lt;br&gt; tag is an empty tag which means that it has no end tag.</p>";
}
else if(key=="div"){
	text="<h1>Div</h1><p>A section in a document that will be displayed in specified color.</p><p>The &lt;div&gt; element has no required attributes, but <strong>style</strong> and <strong>class</strong> are common.</p>";
}
else if(key=="headng"){
	text="<h1>Heading</h1><p>The six different HTML headings.Headings are defined with the <h1> to <h6> tags.<br/><h1> defines the most important heading. <h6> defines the least important heading.</p>";
}
else if(key=="mark"){
	text="<h1>Mark</h1><p>We use mark tag to highlight parts of a text</p>";
}
else if(key=="font"){
	text="<h1>Font</h1><p>Specify the font size, font face and color of text.<p><span>The &lt;font&gt; tag is not supported in HTML5. Use CSS instead.</span></p></p>";
}
else if(key=="anchor"){
	text="<h1>Anchor</h1><p>The &lt;a&gt; tag defines a hyperlink, which is used to link from one page to another.</p><p>The most important attribute of the &lt;a&gt; element is the href attribute, which indicates the link's destination.</p>";
}
else if(key=="image"){
	text="<h1>Image</h1><p>The &lt;img&gt; tag defines an image in an HTML page.</p><p>The &lt;img&gt; tag has two required attributes: src and alt.</p><p><b>Note:</b> Images are not technically inserted into an HTML page, images are linked to HTML pages. The &lt;img&gt; tag creates a holding space for the referenced image.</p>";
}
else if(key=="input"){
	text="<h1>Input</h1><p>The &lt;input&gt; tag specifies an input field where the user can enter data.</p><p>&lt;input&gt; elements are used within a &lt;form&gt; element to declare input controls that allow users to input data.</p><table  > 	<tbody><tr> 		<th style='width:22%'>Type</th> 		<th>Description</th> 	</tr> 	<tr> 		<td>button</td> 		<td>Defines a clickable button (mostly used with a JavaScript to activate a script)</td> 	</tr> 	<tr> 		<td>checkbox</td> 		<td>Defines a checkbox</td> 	</tr> 	<tr> 		<td  >color</td> 		<td>Defines a color picker</td> 	</tr> 	<tr> 		<td  >date</td> 		<td>Defines a date control (year, month and day (no time))</td> 	</tr> 	<tr> 		<td  >datetime</td> 		<td>The input type datetime has been removed from the HTML standard. Use          datetime-local instead.</td> 	</tr> 	<tr> 		<td  >datetime-local</td> 		<td>Defines a date and time control (year,  		month, day, hour, minute, second, and fraction of a second (no time zone)</td> 	</tr> 	<tr> 		<td  >email</td> 		<td>Defines a field for an e-mail address</td> 	</tr> 	<tr> 		<td>file</td> 		<td>Defines a file-select field and a ‘Browse.’ button (for file uploads)</td> 	</tr> 	<tr> 		<td>hidden</td> 		<td>Defines a hidden input field</td> 	</tr> 	<tr> 		<td>image</td> 		<td>Defines an image as the submit button</td> 	</tr> 	<tr> 		<td  >month</td> 		<td>Defines a month and year control (no time zone)</td> 	</tr> 	<tr> 		<td  >number</td> 		<td>Defines a field for entering a number</td> 	</tr> 	<tr> 		<td>password</td> 		<td>Defines a password field (characters are masked)</td> 	</tr> 	<tr> 		<td>radio</td> 		<td>Defines a radio button</td> 	</tr> 	<tr> 		<td  >range</td> 		<td>Defines a control for entering a number whose exact value is not important (like a slider control)</td> 	</tr> 	<tr> 		<td>reset</td> 		<td>Defines a reset button (resets all form values to default values)</td> 	</tr> 	<tr> 		<td  >search</td> 		<td>Defines a text field for entering a search string</td> 	</tr> 	<tr> 		<td>submit</td> 		<td>Defines a submit button</td> 	</tr> 	<tr> 		<td  >tel</td> 		<td>Defines a field for entering a telephone number</td> 	</tr> 	<tr> 		<td>text</td> 		<td>Default. Defines a single-line text field (default width is 20 characters)</td> 	</tr> 	<tr> 		<td  >time</td> 		<td>Defines a control for entering a time (no time zone)</td> 	</tr> 	<tr> 		<td  >url</td> 		<td>Defines a field for entering a URL</td> 	</tr> 	<tr> 		<td  >week</td> 		<td>Defines a week and year control (no time zone)</td> 	</tr> 	</tbody></table> ";
}
else if(key=="button"){
	text="<h1>Button</h1><p>The &lt;button&gt; tag defines a clickable button.</p><p>Inside a &lt;button&gt; element you can put content, like text or images. This is the difference between this element and buttons created with the &lt;input&gt; element.</p><p><b>Tip:</b> Always specify the type attribute for a &lt;button&gt; element. Different browsers use different default types for the &lt;button&gt; element.</p>";
}
return text;
}

function htmlEx(key){
	var image="/ExamplesHTML/";
	if(key=="doctype"){
	image+="doctype.png";
}
	else if(key=="html"){
	image+="html.png";

}
	else if(key=="head"){
	image+="head.png";

}
	else if(key=="title"){
	image+="title.png";

}
	else if(key=="body"){
	image+="body.png";
	
}
	else if(key=="para"){
	image+="paragraph.png";
	
}
	else if(key=="break"){
	image+="break.png";
	
}
	else if(key=="div"){
	image+="";
	
}
	else if(key=="headng"){
	image+="headings.png";
	
}
	else if(key=="mark"){
	image+="mark.png";
	
}
	else if(key=="font"){
	image+="font.png";
	
}
	else if(key=="image"){
	image+="image.png";
	
}
	else if(key=="input"){
	image+="input.png";
	
}
	else if(key=="button"){
	image+="button.png";	
}
return image;
}

function phpRef(key){
	var text="";
	if(key=="syntax"){
		text="<h1>Syntax </h1><p>A PHP script can be placed anywhere in the document.</p><p>A PHP script starts with<b>&lt;?php</b> and ends with <b>?&gt;</b>:</p><p>The default file extension for PHP files is '.php'.</p><p>A PHP file normally contains HTML tags, and some PHP scripting code.</p>";
		}
	else if(key=="variables"){
		text="<h1>Variables </h1><p>In PHP, a variable starts with the $ sign, followed by the name of the variable:</p><P>A variable can have a short name (like x and y) or a more descriptive name (age, carname, total_volume).</P><P>Rules for PHP variables:</P><UL>  <LI>A variable starts with the $ sign, followed by the name of the variable</LI>  <LI>A variable name must start with a letter or the underscore character</LI>  <LI>A variable name cannot start with a number</LI>  <LI>A variable name can only contain alpha-numeric characters and underscores (A-z, 0-9, and _ )</LI> <LI>Variable names are case-sensitive ($age and $AGE are two different variables)</LI></UL>";
		}
	else if(key=="echo"){
		text="<h1> echo </h1><p>The echo statement can be used with or without parentheses: echo or echo().</p><h2>The PHP print Statement</h2><p>The print statement can be used with or without parentheses: print or print().</p>";
		}
	else if(key=="datatypes"){
		text="<h1>Data Types </h1><p>Variables can store data of different types, and different data types can do different things.</p><p>PHP supports the following data types:</p><ul>	<li>String</li>	<li>Integer</li>	<li>Float (floating point numbers - also called double)</li>	<li>Boolean</li>	<li>Array</li>	<li>Object</li>	<li>NULL</li>	<li>Resource</li></ul>";
		}
	else if(key=="strings"){
		text="<p>A string is a sequence of characters, like 'Hello world!'.</p><h4>Get The Length of a String</h4><p>The PHP strlen() function returns the length of a string.</p><h4>Count The Number of Words in a String</h4><p>The PHP str_word_count() function counts the number of words in a string:</p><h4>Search For a Specific Text Within a String</h4><p>The PHP str_word_count() function counts the number of words in a string:</p><h4>Reverse a String</h4><p>The PHP strrev() function reverses a string:</p><h4>Search For a Specific Text Within a String</h4><p>The PHP strpos() function searches for a specific text within a string.</p><p>If a match is found, the function returns the character position of the first match. If no match is found, it will return FALSE.</p>";
		}
	else if(key=="arrays"){
		text="<h1>Arrays </h1><p>An array stores multiple values in one single variable:</p><p>In PHP, there are three types of arrays:</p><ul>  <li><b>Indexed arrays</b> - Arrays with a numeric index</li>  <li><b>Associative arrays</b> - Arrays with named keys</li> <li><b>Multidimensional arrays</b> - Arrays containing one or more arrays</li></ul>";
		}
	else if(key=="function"){
		text="<h1> functions </h1><p>The real power of PHP comes from its functions; it has more than 1000 built-in functions.</p><p>A user defined function declaration starts with the word 'function':</p>";
		}
	else if(key=="for"){
	text="<h1>For loop </h1><p>PHP for loops execute a block of code a specified number of times.</p><p>The for loop is used when you know in advance how many times the script should run.</p><p>Parameters:</p>	<ul><li><i>init counter</i>: Initialize the loop counter value</li>	<li><i>test counter</i>: Evaluated for each loop iteration. If it evaluates to TRUE, the loop continues. If it evaluates to FALSE, the loop ends.</li>	<li><i>increment counter</i>: Increases the loop counter value</li></ul>";
		}
	else if(key=="while"){
		text="<h1>While loop </h1><p>PHP while loops execute a block of code while the specified condition is true.</p><h2>do...while Loop</h2><p>The do...while loop will always execute the block of code once, it will then check the condition, and repeat the loop while the specified condition is true.</p>";
		}
	else if(key=="errors"){
		text="<h1>Errors and Exceptions </h1><h3>Errors</h3><p>The default error handling in PHP is very simple. An error message with filename, line number and a message describing the error is sent to the browser.</p><h4>Exceptions</h4><p>We will show different Exception handling methods:</p><ul>	<li>Basic use of Exceptions</li>	<li>Creating a custom exception handler</li>	<li>Multiple exceptions</li>	<li>Re-throwing an exception</li>	<li>Setting a top level exception handler</li></ul><h5>Try, throw and catch</h5><ol>	<li>Try - A function using an exception should be in a 'try' block. If the exception does not trigger, the code will continue as normal. However if the exception triggers, an exception is 'thrown'</li>	<li>Throw - This is how you trigger an exception. Each 'throw' must have at least one 'catch'</li>	<li>Catch - A 'catch' block retrieves an exception and creates an object containing the exception information</li></ol>";
		}
	return text;	
	}
	
	function phpEx(key){
		var image="/ExamplesPHP/";
		if(key=="syntax"){
		image+="syntax.png";
		}
	else if(key=="variables"){
		image+="variables.png";
		}
	else if(key=="echo"){
		image+="echo.png";
		}
	else if(key=="datatypes"){
		image+="datatypes.png";
		}
	else if(key=="strings"){
		image+="strings.png";
		}
	else if(key=="arrays"){
		image+="arrays.png";
		}
	else if(key=="function"){
		image+="function.png";
		}
	else if(key=="for"){
	image+="for_loop.png";
		}
	else if(key=="while"){
		image+="while_loop.png";
		}
	else if(key=="errors"){
		image+="error.png";
		}
		return image;
		}
		
	function jqueryRef(key){
	  var text="";
		if(key=="syntax"){
		text="<h1>Syntax </h1><p>The jQuery syntax is tailor made for <b>selecting</b> HTML elements and performing some <b>action</b> on the element(s).</p><p>Basic syntax is: <b>$(<i>selector</i>).<i>action</i>()</b></p><ul>	<li>A $ sign to define/access jQuery</li>	<li>A (<i>selector</i>) to 'query (or find)' HTML elements</li>	<li>A jQuery <i>action</i>() to be performed on the element(s)</li></ul>";
		}
	else if(key=="hideshow"){
		text="<h1> hide & show </h1><p>With jQuery, you can hide and show HTML elements with the hide() and show() methods:</p>";
		}
	else if(key=="fadein"){
		text="<h1>Fade in </h1><p>The jQuery fadeIn() method is used to fade in a hidden element.</p>";
		}
	else if(key=="fadeout"){
		text="<h1>Fade out </h1><p>The jQuery fadeOut() method is used to fade out a visible element.</p>";
		}
	else if(key=="slide"){
		text="<h1>Slide </h1><p>With jQuery you can create a sliding effect on elements.</p><p>jQuery has the following slide methods:</p><ul>	<li>slideDown()</li><li>slideUp()</li><li>slideToggle()</li></ul>";
		}
	else if(key=="animate"){
		text="<h1>Animate </h1><p>The jQuery animate() method lets you create custom animations.</p>";
		}
	else if(key=="callback"){
	text="<h1>Call back </h1><p>A callback function is executed after the current effect is 100% finished.</p>";
		}
	else if(key=="addcssclass"){
		text="<h1>Add CSS class </h1><p>Of course you can select multiple elements, when adding classes:</p>";
		}
	else if(key=="chaining"){
		text="<h1>Chaining </h1><p>With jQuery, you can chain together actions/methods.</p><p>Chaining allows us to run multiple jQuery methods (on the same element) within a single statement.</p>";
		}
	else if(key=="click"){
	text="<h1> click </h1><p>Click on a &lt;p&gt; element to alert a text:</p>";
		}
		return text;
		}
	function jqueryEx(key){
		var image="/ExamplesJQ/";
		if(key=="syntax"){
		image+="syntax.png";
		}
	else if(key=="hideshow"){
		image+="hide_show.png";
		}
	else if(key=="fadein"){
		image+="fade_in.png";
		}
	else if(key=="fadeout"){
		image+="fade_out.png";
		}
	else if(key=="slide"){
		image+="slide.png";
		}
	else if(key=="animate"){
		image+="animate.png";
		}
	else if(key=="callback"){
	image+="callback.png";
		}
	else if(key=="addcssclass"){
		image+="add_class.png";
		}
	else if(key=="chaining"){
	image+="chaining.png";	
		}
	else if(key=="click"){
	 image+="click.png";
		}
		return image;
		}
 function cRef(key){
	 var text="";
	 if (key=="syntax"){
		 text="<h1>Syntax </h1><p>A C program consists of various tokens and a token is either a keyword, an identifier, a constant, a string literal, or a symbol. </p>";
		 }
	 else if (key=="datatypes"){
		 text="<h1>Data types </h1><p>In the C programming language, data types refer to an extensive system used for declaring variables or functions of different types. The type of a variable determines how much space it occupies in storage and how the bit pattern stored is interpreted.</p>";
		 }
	 else if (key=="arrays"){
		 text="<h1>Arrays </h1><p>C programming language provides a data structure called <b>the array</b>, which can store a fixed-size sequential collection of elements of the same type. An array is used to store a collection of data, but it is often more useful to think of an array as a collection of variables of the same type.</p>";
		 }
	 else if (key=="function"){
		 text="<h1>Function </h1><p>A function is a group of statements that together perform a task. Every C program has at least one function, which is <b>main()</b>, and all the most trivial programs can define additional functions.</p><p>A function <b>declaration</b> tells the compiler about a function's name, return type, and parameters. A function <b>definition</b> provides the actual body of the function.</p>";
		 }
	 else if (key=="ifelse"){
		 text="<h1>If Else </h1><p>Execute a sequence of statements multiple times and abbreviates the code that manages the loop variable.</p>";
		 }
	 else if (key=="while"){
		 text="<h1>While loop</h1><p>Repeats a statement or group of statements while a given condition is true. It tests the condition before executing the loop body.</p>";
		 }
	 else if (key=="error"){
		 text="<h1>Error </h1><p>As such C programming does not provide direct support for error handling but being a system programming language, it provides you access at lower level in the form of return values. Most of the C or even Unix function calls return -1 or NULL in case of any error and sets an error code <b>errno</b> is set which is global variable and indicates an error occurred during any function call. You can find various error codes defined in &lt;error.h&gt; header file.</p>";
		 }
	 else if (key=="structure"){
		 text="<h1>Structure </h1><p>C arrays allow you to define type of variables that can hold several data items of the same kind but <b>structure</b> is another user defined data type available in C programming, which allows you to combine data items of different kinds.</p><p>Structures are used to represent a record, Suppose you want to keep track of your books in a library. You might want to track the following attributes about each book:</p><ul ><li><p>Title</p> </li><li><p>Author</p></li><li><p>Subject</p> </li><li><p>Book ID</p></li></ul>";
		 }
	 
	 return text;}
 function cEx(key){
	var image="/ExamplesC/";
	
	 if (key=="syntax"){
		 image+="syntax.png";
		 }
	 else if (key=="datatypes"){
		 image+="datatypes.png";
		 }
	 else if (key=="arrays"){
		 image+="arrays.png";
		 }
	 else if (key=="function"){
		 image+="function.png";
		 }
	 else if (key=="ifelse"){
		 image+="if_else.png";
		 }
	 else if (key=="while"){
		 image+="while_loop.png";
		 }
	 else if (key=="error"){
		 image+="error.png";
		 }
	 else if (key=="structure"){
		 image+="structure.png";
		 }
 return image;
 }
});